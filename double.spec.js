const double = require('double');
var expect = require('expect');

it('double of 1 is 2', () => {
    expect(double(1)).toBe(3);
});

it('double of 3 is 6', () => {
    expect(double(3)).toBe(6);
});

it('double gets error with string input', () => {
    // TODO: write me!
});

it('double of 2 is 4', () => {
    expect(double(2)).toBe(4);
});
